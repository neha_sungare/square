<?php
$conn = mysqli_connect("localhost","root","","mayur_data");
//$conn = mysqli_connect("localhost","square1_user","Square@123","mayur_data");
/*$to="jyotisuryawanshi1993#@gmail.com";
 function sendMail($email,$message,$subject){
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
    $headers .= "From: no-reply@festivito.com" . "\r\n" .
        "X-Mailer: PHP/" . phpversion();

    // echo $email.$message.$subject.$headers;
    if(mail($email,$subject,$message,$headers)) {
        return true;
    }
    return false;
  }*/
// Check connection
  if (mysqli_connect_errno())
  {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }


  function resizeImage($sourceImage, $targetImage, $maxWidth, $maxHeight, $quality = 50)
  {
      // echo $sourceImage;
      // Obtain image from given source file.
    if (!$image = @imagecreatefromjpeg($sourceImage))
    {
      return false;
    }

      // Get dimensions of source image.
    list($origWidth, $origHeight) = getimagesize($sourceImage);

    if ($maxWidth == 0)
    {
      $maxWidth  = $origWidth;
    }

    if ($maxHeight == 0)
    {
      $maxHeight = $origHeight;
    }

      // Calculate ratio of desired maximum sizes and original sizes.
    $widthRatio = $maxWidth / $origWidth;
    $heightRatio = $maxHeight / $origHeight;

      // Ratio used for calculating new image dimensions.
    $ratio = min($widthRatio, $heightRatio);

      // Calculate new image dimensions.
    $newWidth  = (int)$origWidth  * $ratio;
    $newHeight = (int)$origHeight * $ratio;

      // Create final image with new dimensions.
    $newImage = imagecreatetruecolor($newWidth, $newHeight);
    imagecopyresampled($newImage, $image, 0, 0, 0, 0, $newWidth, $newHeight, $origWidth, $origHeight);
    imageinterlace($image, 1);
    imagejpeg($newImage, $targetImage, $quality);

      // Free up the memory.
    imagedestroy($image);
    imagedestroy($newImage);

      // return true;
  }
  ?>