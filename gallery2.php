<?php include 'config.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<title>Squre1 </title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="" />
    <link rel="shortcut icon" type="image/x-icon" href="images/fav.png">

	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- bootstrap-css -->
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<!--// bootstrap-css -->
	<!-- css -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<!--// css -->

	<link rel="stylesheet" href="css/lightbox.css">
	<link rel="stylesheet" href="css/gallery-grid.css">
	<!-- font-awesome icons -->
	<link href="css/font-awesome.css" rel="stylesheet"> 
	<!-- //font-awesome icons -->

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">

	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/gal.js"></script>
	<script src="js/bootstrap.js"></script>


	<!--animate-->
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<script src="js/wow.min.js"></script>
	<script>
		new WOW().init();
	</script>
	<!--//end-animate-->
</head>
<body>
	<div class="header">
		<div class="top-header">
			<div class="container">
				<div class="top-header-info">
					<div class="top-header-left wow fadeInLeft animated" data-wow-delay=".5s">
						<p>We Create .You Experience.</p>
					</div>
					<div class="top-header-right wow fadeInRight animated" data-wow-delay=".5s">
						<div class="top-header-right-info">

						</div>
						<div class="social-icons">
							<ul>
<li><a class="twitter facebook" href="https://www.facebook.com/square1eventsandmedia/"><i class="fa fa-facebook"></i></a></li>
<li><a class="twitter" href="https://www.instagram.com/square1events"><i class="fa fa-instagram"></i></a></li>
<li><a class="twitter" href="https://www.linkedin.com/company/square-1-events-and-media/"><i class="fa fa-linkedin"></i></a></li>
</ul>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
		</div>
		<div class="bottom-header">
			<div class="container">
				<div class="logo wow fadeInDown animated" data-wow-delay=".5s">
					<h1><a href="index.php"><img src="images/logo.jpg" alt="" /></a></h1>
				</div>
				<div class="top-nav wow fadeInRight animated" data-wow-delay=".5s">
					<nav class="navbar navbar-default">
						<div class="container">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">Menu           
							</button>
						</div>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li><a href="index.php" >Home</a></li>
								<li><a href="about.php">About us</a></li>
								<li><a  href="service.php">Event Services</a></li>
								<li><a href="gallery.php" class="active" >Gallery</a>

								</li> 
								<li><a href="client.php">Clients</a></li>

								<li><a href="contact.php">Contact</a></li>
							</ul> 
							<div class="clearfix"> </div>
						</div>  
					</nav>    
				</div>
			</div>
		</div>
	</div>

	<!-- gallery -->
	<div class="gallery">

		<div class="container">
			<div class="row">
				<div class="gallery col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h1 class="gallery-title">Gallery</h1>
					<div class="border-line2"></div>
				</div>

				<div align="center">


					<div class="navbar navbar-default " role="navigation">
						<div class="container no-padding">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>

							</div>
							<div class="collapse navbar-collapse" style=" background: #fff7b1;">

								<ul class="nav navbar-nav" id = "gallery-head">
									<li class="active"><a onclick = "$('#head').html('All');" href="javascript:;" class="dropdown-toggle btn btn-default filter-button" data-filter="all" data-toggle="dropdown" aria-expanded="false">All</a>
									</li>
									<?php 
									$res = $conn->query('select * from category');     
									if($res->num_rows){
										while ($row = $res->fetch_assoc()) {
											$d = [];
											$resSub = $conn->query('select * from subcategory where category_id = '.$row['category_id']);
											if($resSub->num_rows){
												while ($rowSub = $resSub->fetch_assoc()) {
													array_push($d, '<li><a onclick = "$(\'#head\').html(\''.$rowSub['subcategoryname'].'\');" href="javascript:;" class="btn btn-default filter-button" data-filter="sub-'.$rowSub['subcategory_id'].'">'.$rowSub['subcategoryname'].'</a></li>');
												} 
											}
											echo '<li><a onclick = "$(\'#head\').html(\''.$row['category_name'].'\');" href="javascript:;" class="dropdown-toggle btn btn-default filter-button " data-filter="cat-'.$row['category_id'].'" data-toggle="dropdown">'.$row['category_name'].'<b class="caret"></b></a><ul class="dropdown-menu multi-level">'.implode('',$d).'</ul>
											</li>'; 
										}
									}
									?>
								</ul>
							</div><!--/.nav-collapse -->
						</div>
					</div>
				</div>
				<br/>
				<div class="tz-gallery">
					<div class="row">
						<h1 class="header-gallery" id = "head"></h1>
						<?php 
						$res = $conn->query('select * from gallary inner join category on category.category_id = gallary.category_id');      
						if($res->num_rows){
							while ($row = $res->fetch_assoc()) {
								echo '<div class="col-sm-6 col-md-4 gallery_product all   filter cat-'.$row['category_id'].' sub-'.$row['subcategory_id'].'">'
								.'<a class=" lightbox " href="admin/'.$row['image'].'">'
								.'<img class="img-responsive" src="admin/'.$row['image'].'" alt="">'
								.'</a>'
								.'<a class="lightbox" href="images/s1.jpg"></a>'
								.'<a href="javascript:;" class = "btn n" data-toggle="modal" data-target="#cat-'.$row['gallary_id'].'Modal"><div class="know">Know More</div></a>'
								.'</div>';
								?>
								<div id="<?php echo 'cat-'.$row['gallary_id'].'Modal'?>" class="modal fade" role="dialog">
									<div class="modal-dialog modal-lg">

										<!-- Modal content-->
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title"></h4>
											</div>
											<div class="modal-body">
												<div class="col-md-7">
													<img class="img3" src="<?php echo 'admin/'.$row['image']; ?>" alt="">
												</div>

												<div class="col-md-5">
													<h3 class="my-3"><?php echo $row['gallery_title']?></h3>
													<?php echo $row['description']?>
												</div>
												<div class="clearfix"></div>
											</div>											
										</div>

									</div>
								</div>
								<?php

							}
						}
						?>

					</div>

				</div>
			</div>
		</div>
	</section>
</div>
<!-- //gallery -->
<!-- footer -->

<div class="footer agileits">
	<div class="container">
		<div class="footer-grids w3l-agile">
			<div class="col-md-3 footer-nav agile-w3layouts wow fadeInLeft animated" data-wow-delay=".5s">
				<h4>Quick Links</h4>
				<ul>
					<li><a href="index.php" class="">Home</a></li>
					<li><a href="about.php">About us</a></li>
					<li><a  href="service.php">Event Services</a></li>
					<li><a href="gallery.php" >Gallery</a>

					</li> 
					<li><a href="client.php">Clients</a></li>

					<li><a href="contact.php">Contact</a></li>
				</ul>
			</div>
			<div class="col-md-5 footer-nav agile-w3layouts wow fadeInUp animated" data-wow-delay=".5s">
				<h4>Let's stay in touch</h4>
				<p>Subscribe for updates, special offers and more.</p>
				<form action="#" method="post">
					<input type="email" id="mc4wp_email" name="email" placeholder="Enter your email here" required="">
					<input type="submit" value="Subscribe">
				</form>
			</div>
			<div class="col-md-4 footer-nav agile-w3layouts wow fadeInRight animated" data-wow-delay=".5s">
				<img class="img1" src="images/foo.png">
			</div>
			<div class="clearfix"> </div>
		</div>

	</div>

</div>
<!-- //footer -->
<div class="copyright " data-wow-delay=".5s">
	<p>© 2018 Square 1 . All Rights Reserved . Design by <a href="http://sungare.com/"> Sungare technologies</a></p>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<script>
	baguetteBox.run('.tz-gallery');
	// $('#gallery-head').find('li').each(function(k,v){		
	// 	$(v).on({
	// 		'click' : function(){
	// 			$('#head').html($(this).find('a').html());
	// 		}
	// 	});
	// });
</script>
</body> 
</html>