<!DOCTYPE html>
<html>
<head>
    <title>Squre1 </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="keywords" content="" />
    <link rel="shortcut icon" type="image/x-icon" href="images/fav.png">
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- bootstrap-css -->
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <!--// bootstrap-css -->
    <!-- css -->
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
    <!--// css -->
    <!-- font-awesome icons -->
    <link href="css/font-awesome.css" rel="stylesheet"> 
    <!-- //font-awesome icons -->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/move-top.js"></script>
    <script type="text/javascript" src="js/easing.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event){     
                event.preventDefault();
                $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
            });
        });
    </script>   
    <!--animate-->
    <link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
    <script src="js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>
    <!--//end-animate-->
</head>
<body>
    <div class="header">
        <div class="top-header">
            <div class="container">
                <div class="top-header-info">
                    <div class="top-header-left wow fadeInLeft animated" data-wow-delay=".5s">
                        <p>We Create .You Experience.</p>
                    </div>
                    <div class="top-header-right wow fadeInRight animated" data-wow-delay=".5s">
                        <div class="top-header-right-info">

                            <!-- <ul>
                                <li><a href="login.html">Login</a></li>
                                <li><a href="signup.html">Sign up</a></li>
                            </ul> -->
                        </div>
                        <div class="social-icons">
                            <ul>
                             <li><a class="twitter facebook" href="https://www.facebook.com/square1eventsandmedia/"><i class="fa fa-facebook"></i></a></li>
                             <li><a class="twitter" href="https://www.instagram.com/square1events"><i class="fa fa-instagram"></i></a></li>
                             <li><a class="twitter" href="https://www.linkedin.com/company/square-1-events-and-media/"><i class="fa fa-linkedin"></i></a></li>
                         </ul>
                     </div>
                     <div class="clearfix"> </div>
                 </div>
                 <div class="clearfix"> </div>
             </div>
         </div>
     </div>
     <div class="bottom-header">
        <div class="container">
            <div class="logo wow fadeInDown animated" data-wow-delay=".5s">
                <h1><a href="index.php"><img src="images/logo.jpg" alt="" /></a></h1>
            </div>
            <div class="top-nav wow fadeInRight animated" data-wow-delay=".5s">
                <nav class="navbar navbar-default">
                    <div class="container">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">Menu                       
                        </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="index.php" >Home</a></li>
                            <li><a href="about.php">About us</a></li>
                            <li><a  href="service.php">Event Services</a></li>
                            <li><a href="gallery.php" >Gallery</a>

                            </li>   
                            <li><a href="client.php " class="active">Clients</a></li>

                            <li><a href="contact.php">Contact</a></li>
                        </ul>   
                        <div class="clearfix"> </div>
                    </div>  
                </nav>      
            </div>
        </div>
    </div>
</div>

<!-- gallery -->
<div class="cli">

    <div class="container">
      <?php 
      include("config.php");

      $sql="SELECT * FROM client";
      $result = mysqli_query($conn, $sql);
      while ($row_img = mysqli_fetch_assoc($result)) 
      {                      
          ?>
          <div class="gallery-grids">
            <div class="col-md-3 gallery-grid wow fadeInUp animated" data-wow-delay=".5s">
                <div class="grid">
                    <figure class="effect-apollo">
                        <a>
                            
                         <img src="admin/<?php echo $row_img['image'];?>" alt=""/>
                         <figcaption>
                         </figcaption>   
                     </a>
                 </figure>
             </div>
         </div>
         <?php } ?>
                    <!-- <div class="col-md-3 gallery-grid wow fadeInUp animated" data-wow-delay=".5s">
                        <div class="grid">
                            <figure class="effect-apollo">
                                <a>
                                    <img src="images/g2.jpg" alt="" />
                                    <figcaption>
                                        
                                    </figcaption>   
                                </a>
                            </figure>
                        </div>
                    </div> -->
<!--                    <div class="col-md-3 gallery-grid wow fadeInUp animated" data-wow-delay=".5s">
    <div class="grid">
        <figure class="effect-apollo">
            <a    
  
>
                <img src="images/g3.jpg" alt="" />
                <figcaption>
                    
                </figcaption>       
            </a>
        </figure>
    </div>
</div>
<div class="col-md-3 gallery-grid wow fadeInUp animated" data-wow-delay=".5s">
    <div class="grid">
        <figure class="effect-apollo">
            <a    
 
>
                <img src="images/g4.jpg" alt="" />
                <figcaption>
                    
                </figcaption>   
            </a>
        </figure>
    </div>
</div>
<div class="col-md-3 gallery-grid wow fadeInUp animated" data-wow-delay=".5s">
    <div class="grid">
        <figure class="effect-apollo">
            <a    
  
>
                <img src="images/g5.jpg" alt="" />
                <figcaption>
                
                </figcaption>   
            </a>
        </figure>
    </div>
</div>
<div class="col-md-3 gallery-grid wow fadeInUp animated" data-wow-delay=".5s">
    <div class="grid">
        <figure class="effect-apollo">
            <a    
   
>
                <img src="images/g6.jpg" alt="" />
                <figcaption>
                    
                </figcaption>       
            </a>
        </figure>
    </div>
</div>
<div class="col-md-3 gallery-grid wow fadeInUp animated" data-wow-delay=".5s">
    <div class="grid">
        <figure class="effect-apollo">
            <a    
 
>
                <img src="images/g7.jpg" alt="" />
                <figcaption>
                    
                </figcaption>   
            </a>
        </figure>
    </div>
</div>
-->
<div class="clearfix"> </div>

</div>
</div>
</div>
<!-- //gallery -->
<!-- footer -->

<div class="footer agileits">
    <div class="container">
        <div class="footer-grids w3l-agile">
            <div class="col-md-3 footer-nav agile-w3layouts wow fadeInLeft animated" data-wow-delay=".5s">
                <h4>Quick Links</h4>
                <ul>
                    <li><a href="index.php" class="">Home</a></li>
                    <li><a href="about.php">About us</a></li>
                    <li><a  href="service.php">Event Services</a></li>
                    <li><a href="gallery.php" >Gallery</a>

                    </li>   
                    <li><a href="client.php">Clients</a></li>
                    
                    <li><a href="contact.php">Contact</a></li>
                </ul>
            </div>
            <div class="col-md-5 footer-nav agile-w3layouts wow fadeInUp animated" data-wow-delay=".5s">
                <h4>Let's stay in touch</h4>
                <p>Subscribe for updates, special offers and more.</p>
                <form action="#" method="post">
                    <input type="email" id="mc4wp_email" name="email" placeholder="Enter your email here" required="">
                    <input type="submit" value="Subscribe">
                </form>
            </div>
            <div class="col-md-4 footer-nav agile-w3layouts wow fadeInRight animated" data-wow-delay=".5s">
                <img class="img1" src="images/foo.png">
            </div>
            <div class="clearfix"> </div>
        </div>

    </div>

</div>
<!-- //footer -->
<div class="copyright " data-wow-delay=".5s">
    <p>© 2018 Square 1 . All Rights Reserved . Design by <a href="http://sungare.com/"> Sungare technologies</a></p>
</div>
</body> 
</html>