<?php
include '../config.php';

$s = $conn->query("select * from subcategory where subcategory_id = ".$_POST['id']);
$sr = $s->fetch_assoc();



$qry = "select * from gallary where subcategory_id = ".$_POST['id'];
$res = $conn->query($qry);

$data = ['head' => $sr['subcategoryname'], 'data' => '', 'id' => $sr['category_id']];
if($res->num_rows){
	while ($row = $res->fetch_assoc()) {	
		$next = '';
		$prev = '';
		$gPre = $conn->query("select * from gallary where gallary_id = (select max(gallary_id) from gallary where gallary_id < ".$row['gallary_id'].")");
		$gNext = $conn->query("select * from gallary where gallary_id = (select min(gallary_id) from gallary where gallary_id > ".$row['gallary_id'].")");
		
		if($gPre->num_rows){
			$gallaryPre = $gPre->fetch_assoc();
			$prev = '<button class = "modal-button modal-left" onclick = "$(\'#cat-'.$row['gallary_id'].'Modal\').modal(\'toggle\');$(\'#cat-'.$gallaryPre['gallary_id'].'Modal\').modal(\'toggle\');">Previous</button>';
		}

		if($gNext->num_rows){
			$gallaryNext = $gNext->fetch_assoc();
			$next = '<button class = "modal-button pull-right modal-right" onclick = "$(\'#cat-'.$row['gallary_id'].'Modal\').modal(\'toggle\');$(\'#cat-'.$gallaryNext['gallary_id'].'Modal\').modal(\'toggle\');">Next</button>';
		}
		
		$data['data'] .= '<div class="col-sm-6 col-md-4 gallery_product">'
		.'<a class="lightbox" href="javascript:;" data-toggle="modal" data-target="#cat-'.$row['gallary_id'].'Modal">'
		.'<img class="img-responsive" src="'.$row['image'].'" alt="Park">'
		.'</a>'
		.'<a class="lightbox" href="images/s1.jpg"></a>'
		.'</div>'
		.'<div id="cat-'.$row['gallary_id'].'Modal" class="modal fade" role="dialog">'
		.'<div class="modal-dialog modal-lg">'
			.'<div class="modal-content">'
				.'<div class="modal-header">'
					.'<button type="button" class="close" data-dismiss="modal">&times;</button>'
					.'<h4 class="modal-title"></h4>'
				.'</div>'
				.'<div class="modal-body">'
					.'<div class="col-md-12">'
						.'<h3 class="my-3">'.$row['gallery_title'].'</h3>'
						// .$row['description']
					.'</div>'
					.'<div class="col-md-12">'
						.'<img class="img3" src="'.$row['image'].'" alt="">'
					.'</div>'
					.'<div class="clearfix"></div>'
				.'</div>'					
				.$prev
				.$next
				.'<div class="clearfix"></div>'
			.'</div>'
		.'</div>'
		.'</div>';
	}
}else{
	$data['data'] = '<div class="header-gallery text-center"><h3>No Images Found</h3></div>';
}

echo json_encode($data);