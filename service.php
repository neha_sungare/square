<!DOCTYPE html>
<html>
<head>
    <title>Square1 </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="keywords" content="" />
    <link rel="shortcut icon" type="image/x-icon" href="images/fav.png">
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- bootstrap-css -->
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <!--// bootstrap-css -->
    <!-- css -->
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
    <!--// css -->
    <!-- font-awesome icons -->
    <link href="css/font-awesome.css" rel="stylesheet"> 
    <!-- //font-awesome icons -->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/move-top.js"></script>
    <script type="text/javascript" src="js/easing.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event){     
                event.preventDefault();
                $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
            });
        });
    </script>   
    <!--animate-->
    <link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
    <script src="js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>
    <!--//end-animate-->
</head>
<body>
    <div class="header">
        <div class="top-header">
            <div class="container">
                <div class="top-header-info">
                    <div class="top-header-left wow fadeInLeft animated" data-wow-delay=".5s">
                        <p>We Create .You Experience.</p>
                    </div>
                    <div class="top-header-right wow fadeInRight animated" data-wow-delay=".5s">
                        <div class="top-header-right-info">

                            <!-- <ul>
                                <li><a href="login.html">Login</a></li>
                                <li><a href="signup.html">Sign up</a></li>
                            </ul> -->
                        </div>
                        <div class="social-icons">
                            <ul>
                              <li><a class="twitter facebook" href="https://www.facebook.com/square1eventsandmedia/"><i class="fa fa-facebook"></i></a></li>
                              <li><a class="twitter" href="https://www.instagram.com/square1events"><i class="fa fa-instagram"></i></a></li>
                              <li><a class="twitter" href="https://www.linkedin.com/company/square-1-events-and-media/"><i class="fa fa-linkedin"></i></a></li>
                          </ul>
                      </div>
                      <div class="clearfix"> </div>
                  </div>
                  <div class="clearfix"> </div>
              </div>
          </div>
      </div>
      <div class="bottom-header">
        <div class="container">
            <div class="logo wow fadeInDown animated" data-wow-delay=".5s">
                <h1><a href="index.php"><img src="images/logo.jpg" alt="" /></a></h1>
            </div>
            <div class="top-nav wow fadeInRight animated" data-wow-delay=".5s">
                <nav class="navbar navbar-default">
                    <div class="container">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">Menu                       
                        </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="index.php" >Home</a></li>
                            <li><a href="about.php">About us</a></li>
                            <li><a  href="service.php " class="active">Event Services</a></li>
                            <li><a href="gallery.php" >Gallery</a>

                            </li>   
                            <li><a href="client.php">Clients</a></li>

                            <li><a href="contact.php">Contact</a></li>
                        </ul>   
                        <div class="clearfix"> </div>
                    </div>  
                </nav>      
            </div>
        </div>
    </div>
</div>

<!-- login -->
<div class="login">
    <div class="row bg-1 ">
      <!--   <img class="img1" src="images/ser.png"> -->
      <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
          <h3 class="text-d" style="margin-top: 32%">WE CREATE </h3>
      </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 ">
       <ul class="menu">
  <li class="one">
    <a href="#">
      <span class="icon">Entertainment<br> Events</span>
    </a>
  </li>
  <li class="two">
    <a href="#">
      <span class="icon">Corporate Events</span>
    </a>
  </li>
  <li class="three">
    <a href="#">
      <span class="icon">Branding And<br> Advertising</span>
    </a>
  </li>
  <li class="four">
    <a href="#">
      <span class="icon">Wedding Events</span>
    </a>
  </li>
  <li class="five">
    <a href="#">
      <span class="icon">Social Events.</span>
    </a>
  </li>
  <li class="six">
    <a href="#">
      <span class="icon">Entertainment<br> Events</span>
    </a>
  </li>
</ul>
<svg height="0" width="0">
  <defs>
    <clipPath clipPathUnits="objectBoundingBox" id="sector">
      <path fill="none" stroke="#111" stroke-width="1" class="sector" d="M0.5,0.5 l0.5,0 A0.5,0.5 0 0,0 0.75,.066987298 z"></path>
    </clipPath>
  </defs>
</svg>

      </div>
       <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
             <h3 class="text-d" style="margin-top: 32%">YOUR EXPIERENCE</h3>
      </div>
    </div>
    <div class="container">
     <?php 
     include("config.php");
     $event_id='';  
     $res = $conn->query("select * from event");      
     if($res->num_rows){
        $k = 0;
        while ($row = $res->fetch_assoc()) {  
            if($k%2 == 0){
                ?>
                <div class="blog-top-grids">
                    <div class="col-md-8 blog-top-left-grid">
                        <div class="left-blog">
                            <div class="blog-left">
                                <div class="blog-left-left wow fadeInUp animated animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">

                                    <a href="#"><img src="admin/<?php echo $row['event_image'];?>" alt=""></a>
                                </div>

                                <div class="clearfix"> </div>
                            </div>

                        </div>

                    </div>
                    <div class="col-md-4 blog-top-right-grid">
                        <div class="Categories wow fadeInUp animated animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">
                            <h3><?php echo $row['event_name'];?></h3>
                            <ul>
                                <li> <a><b><?php echo $row['event_name'];?> :- </b> </a></li>
                                <?php echo $row['event_file']; ?>
                                
                            </ul>
                        </div>


                    </div>
                    <div class="clearfix"> </div>
                </div>

                <?php
            }else{
                ?>                      
                <div class="blog-top-grids">
                    <div class="col-md-4 blog-top-right-grid">
                        <div class="Categories wow fadeInUp animated animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">
                            <h3><?php echo $row['event_name'];?></h3>
                            <ul>
                                <li><a href="#"> <b><?php echo $row['event_name'];?> :- </b> </a></li>
                                <?php echo $row['event_file']; ?>

                            </ul>
                        </div>


                    </div>
                    <div class="col-md-8 blog-top-left-grid">
                        <div class="left-blog">
                            <div class="blog-left">
                                <div class="blog-left-left wow fadeInUp animated animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">

                                    <a href="#"><img src="admin/<?php echo $row['event_image'];?>" alt=""></a>
                                </div>

                                <div class="clearfix"> </div>
                            </div>

                        </div>

                    </div>

                    <div class="clearfix"> </div>
                </div>
                <?php
            }

            $k++;
            ?>
<!--                 <div class="blog-heading w3layouts">

                    <p class="wow fadeInUp animated animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;"></p>
                </div>        -->                         

                <?php } 
            } 
            ?>
            <!--  </div> -->
        </div>
        <!-- //login -->
        <!-- footer -->

        <div class="footer agileits">
            <div class="container">
                <div class="footer-grids w3l-agile">
                    <div class="col-md-3 footer-nav agile-w3layouts wow fadeInLeft animated" data-wow-delay=".5s">
                        <h4>Quick Links</h4>
                        <ul>
                            <li><a href="index.php" class="">Home</a></li>
                            <li><a href="about.php">About us</a></li>
                            <li><a  href="service.php">Event Services</a></li>
                            <li><a href="gallery.php" >Gallery</a>

                            </li>   
                            <li><a href="client.php">Clients</a></li>

                            <li><a href="contact.php">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-md-5 footer-nav agile-w3layouts wow fadeInUp animated" data-wow-delay=".5s">
                        <h4>Let's stay in touch</h4>
                        <p>Subscribe for updates, special offers and more.</p>
                        <form action="#" method="post">
                            <input type="email" id="mc4wp_email" name="email" placeholder="Enter your email here" required="">
                            <input type="submit" value="Subscribe">
                        </form>
                    </div>
                    <div class="col-md-4 footer-nav agile-w3layouts wow fadeInRight animated" data-wow-delay=".5s">
                        <img class="img1" src="images/foo.png">
                    </div>
                    <div class="clearfix"> </div>
                </div>

            </div>

        </div>
        <!-- //footer -->
        <div class="copyright " data-wow-delay=".5s">
            <p>© 2018 Square 1 . All Rights Reserved . Design by <a href="http://sungare.com/"> Sungare technologies</a></p>
        </div>

            <script type="text/javascript">
            
            $(document).ready(function () {
    $('.material-button-toggle').on("click", function () {
        $(this).toggleClass('open');
        $('.option').toggleClass('scale-on');
    });
});
        </script>
    </body> 
    </html>