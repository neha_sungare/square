$('#contactfrm').on('submit', function(e){
    e.preventDefault();
    $('#success_message').fadeIn().html('Checking Data');
    $("#btnadd").prop('disabled', true);
    var fd = new FormData(this);
    $.ajax({
        url: 'add_contact.php',
        type: 'POST',
        data:  fd,
        contentType: false,
        processData:false,
        success: function(data) 
        {
            $('#success_message').fadeIn().html('Your Message Send Sucessfully');
            setTimeout(function() {
                $('#contactfrm')[0].reset();
                 $("#btnadd").prop('disabled', false);
                $('#success_message').fadeOut("slow");
            }, 1000 );
        },
    });
});