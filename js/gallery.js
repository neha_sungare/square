function getCat(){
	$('#gallery').html('<div class="header-gallery text-center"><h3>Loading...</h3></div>');
	$.post('req/fetch-cat.php',function (data){
		$('#gallery').html(data);
		$('#head').html('All Events');
	},'json');
}
function getSub(id){
	$('#gallery').html('<div class="header-gallery text-center"><h3>Loading...</h3></div>');
	$.post('req/fetch-sub.php', {id : id},function (data){
		$('#gallery').html(data.data);
		$('#head').html(data.head+'<button class="btn btn-warning pull-right" onclick = "getCat();">Back</button>');		
	},'json');
}
function getimage(id){
	$('#gallery').html('<div class="header-gallery text-center"><h3>Loading...</h3></div>');
	$.post('req/fetch-image.php', {id : id},function (data){
		$('#gallery').html(data.data);
		$('#head').html(data.head+'<button class="btn btn-warning pull-right" onclick = "getCat();"  style = "margin-left: 15px;">All Events</button> <button class="btn btn-warning pull-right" onclick = "getSub('+data.id+');">Back</button>');
		//console.log(data);
	},'json');
}