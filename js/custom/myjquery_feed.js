// 0/ alert('OK');

$('#name').on('keyup',function()
{
	val =$(this).val();
	if(val.match(/^[a-zA-Z]+$/))
	{
		$('#errorname').hide();
		$(this).css('border-color','');
	}
	else
	{
		$(this).css('border-color','red');
		$('#errorname').show();
		$('#errorname').html('Enter valid name');
		$('#errorname').css('color','red');
	}
});	
$('#email').on('change',function()
{
	val =$(this).val();
	if(val.match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i))
	{
		$('#erroremail').hide();
		$(this).css('border-color','');
	}
	else
	{
		$(this).css('border-color','red');
		$('#erroremail').show();
		$('#erroremail').html('Enter valid Email');
		$('#erroremail').css('color','red');
	}
});

$('#city').on('blur',function()
{
	val =$(this).val();
	// console.log(val);
	if(val != -1)
	{
		$('#errorcity').hide();
		$(this).css('border-color','');
	}
	else
	{
		$(this).css('border-color','red');
		$('#errorcity').show();
		$('#errorcity').html('Select City');
		$('#errorcity').css('color','red');
	}
});
$('#phone').on('keyup',function()
{
	val =$(this).val();
	if(val.match(/^[7/8/9][0-9]{9}$/))
	{
		$('#errorphone').hide();
		$(this).css('border-color','');
	}
	else
	{
		$(this).css('border-color','red');
		$('#errorphone').show();
		$('#errorphone').html('Enter valid Phone No');
		$('#errorphone').css('color','red');
	}
});


$('#contact').on('keyup',function()
{
	val =$(this).val();
	if(val.match(/^[7/8/9][0-9]{9}$/))
	{
		$('#errorcontact').hide();
		$(this).css('border-color','');
	}
	else
	{
		$(this).css('border-color','red');
		$('#errorcontact').show();
		$('#errorcontact').html('Enter valid Contact No');
		$('#errorcontact').css('color','red');
	}
});