
<!DOCTYPE html>
<head>
	<title>Square 1</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Colored Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- bootstrap-css -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- //bootstrap-css -->
	<!-- Custom CSS -->
	<link href="css/style.css" rel='stylesheet' type='text/css' />
	<!-- font CSS -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<!-- font-awesome icons -->
	<link rel="stylesheet" href="css/font.css" type="text/css"/>
	<link href="css/font-awesome.css" rel="stylesheet"> 
	<!-- //font-awesome icons -->
<!-- <script src="js/jquery2.0.3.min.js"></script>
	<-->

	<script type="text/javascript" src="../admin/js/jquery-1.11.1.min.js"></script>
	<script src="js/modernizr.js"></script>
	<script src="js/jquery.cookie.js"></script>
	<script src="js/screenfull.js"></script>

	<script src="//cdn.ckeditor.com/4.5.5/standard/ckeditor.js"></script>
	<script>
		$(function () {
			$('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

			if (!screenfull.enabled) {
				return false;
			}

			$('#toggle').click(function () {
				screenfull.toggle($('#container')[0]);
			});	
		});
	</script>


	<!-- tables -->
	<link rel="stylesheet" type="text/css" href="css/table-style.css" />
	<link rel="stylesheet" type="text/css" href="css/basictable.css" />
	<script type="text/javascript" src="js/jquery.basictable.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#table').basictable();

			$('#table-breakpoint').basictable({
				breakpoint: 768
			});

			$('#table-swap-axis').basictable({
				swapAxis: true
			});

			$('#table-force-off').basictable({
				forceResponsive: false
			});

			$('#table-no-resize').basictable({
				noResize: true
			});

			$('#table-two-axis').basictable();

			$('#table-max-height').basictable({
				tableWrapper: true
			});
		});
	</script>
	<!-- //tables -->
</head>
<body class="dashboard-page">
	<?php require("nav_menu.php"); ?>

	<section class="wrapper scrollable">
		<nav class="user-menu">
			<a href="javascript:;" class="main-menu-access">
				<i class="icon-proton-logo"></i>
				<i class="icon-reorder"></i>
			</a>
		</nav>
		<?php require("header.php");?>

		<div class="main-grid">
			<div class="agile-grids">	
				<!-- input-forms -->
				<div class="grids">
					<div class="progressbar-heading grids-heading">
						<h2>Gallary</h2>
					</div>
					<div class="panel panel-widget forms-panel">
						<div class="forms">
							<div class="form-grids widget-shadow" data-example-id="basic-forms"> 
								<div class="form-title">
									<h4>Gallary Form:</h4>
								</div>
								<div class="form-body">
									<?php 
									include("../config.php");
									$gallary_id=0;
									$sql="SELECT gallary_id FROM gallary";
									$result = mysqli_query($conn, $sql);
									while ($row = mysqli_fetch_assoc($result)) 
									{						
										$gallary_id=$row['gallary_id'];
									}
									$gallary_id=$gallary_id+1;
									?>
									<form action="#" name="frmgallary" id="frmgallary" method="post" enctype="multipart/form-data"> 
										<div class="form-group"> 

											<label for="exampleInputEmail1">Id</label> 
											<input type="text" name="gallary_id" class="form-control" id="gallary_id" placeholder="Enter ID" readonly="readonly" value="<?php echo $gallary_id; ?>"> 
										</div> 
										<div class="form-group"> 

											<label for="exampleInputEmail1">Title</label> 
											<input type="text" name="gallery_title" class="form-control" placeholder="Enter Title for Image"> 
										</div> 
										<div class="form-group">
											<label for="selector1">Category Select</label>
											<div>
												
												<select id = "category_name" name = "category_name" class="form-control1">
													<option>Select Category</option>
													<?php 
													$res = $conn->query('select * from category');			
													if($res->num_rows){
														while ($row = $res->fetch_assoc()) {

															echo '<option value = "'.$row['category_id'].'">'.$row['category_name'].'</option>';
														}
													}
													?>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label for="selector1">Subcategory Select</label>
											<div>
												<select id = "sub" disabled name="subcategory_id" class="form-control1"></select>
												
												
												
											</div>
										</div>
										<div class="form-group">
											<label for="selector1">Description</label>
											<div>
												<textarea name="description" id="des"></textarea>
												<script>
													CKEDITOR.replace( 'des' );
												</script>
											</div>
										</div>
										<div class="form-group"> 
											<label for="exampleInputFile">File input</label> 
											<input type="file" name="img" id="image"> 
											<p class="help-block">Example block-level help text here.</p> 
										</div> 
										<center><font color="black"><div id="success_message" style="display:none;">Data Submitted Sucessfully </div></font></center>
										<button type="submit" name="btnadd" id="btnadd" class="btn btn-primary w3ls-button">Submit</button> 
										
									</form> 
								</div>
							</div>
						</div>
					</div>

				</div>		
				
				<!-- //input-forms -->
			</div>
		</div>
		<script src="../admin/js/gallary.js"></script>

		<!-- footer -->
		<?php require("footer.php") ?>
		<!-- //footer -->
	</section>
<!-- 	<script src="js/bootstrap.js"></script>
-->	
<script type="text/javascript">
	$('#category_name').on({
		'change' : function() {
			$.post('fetch_sub.php',{id : this.value},function(data){				
				$('#sub').prop('disabled',false).html('<option>-Sub Categories-</option>').append(data);
			},'json');
		}
	});

</script>
</body>
</html>

