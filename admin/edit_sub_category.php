
<!DOCTYPE html>
<head>
	<title>Square 1</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Colored Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- bootstrap-css -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<style type="text/css">
	.id-card{
		width: 300px;
	}
	img {
		object-fit: contain;
	}
	</style>
	<!-- //bootstrap-css -->
	<!-- Custom CSS -->
	<link href="css/style.css" rel='stylesheet' type='text/css' />
	<!-- font CSS -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<!-- font-awesome icons -->
	<link rel="stylesheet" href="css/font.css" type="text/css"/>
	<link href="css/font-awesome.css" rel="stylesheet"> 
	<!-- //font-awesome icons -->
<!-- <script src="js/jquery2.0.3.min.js"></script>
	<-->

	<script type="text/javascript" src="../admin/js/jquery-1.11.1.min.js"></script>
	<script src="js/modernizr.js"></script>
	<script src="js/jquery.cookie.js"></script>
	<script src="js/screenfull.js"></script>

	<script>
		$(function () {
			$('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

			if (!screenfull.enabled) {
				return false;
			}

			$('#toggle').click(function () {
				screenfull.toggle($('#container')[0]);
			});	
		});
	</script>


	<!-- tables -->
	<link rel="stylesheet" type="text/css" href="css/table-style.css" />
	<link rel="stylesheet" type="text/css" href="css/basictable.css" />
	<script type="text/javascript" src="js/jquery.basictable.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#table').basictable();

			$('#table-breakpoint').basictable({
				breakpoint: 768
			});

			$('#table-swap-axis').basictable({
				swapAxis: true
			});

			$('#table-force-off').basictable({
				forceResponsive: false
			});

			$('#table-no-resize').basictable({
				noResize: true
			});

			$('#table-two-axis').basictable();

			$('#table-max-height').basictable({
				tableWrapper: true
			});
		});
	</script>
	<!-- //tables -->
</head>
<body class="dashboard-page">
	<?php require("nav_menu.php"); ?>

	<section class="wrapper scrollable">
		<nav class="user-menu">
			<a href="javascript:;" class="main-menu-access">
				<i class="icon-proton-logo"></i>
				<i class="icon-reorder"></i>
			</a>
		</nav>
		<?php require("header.php");?>

		<div class="main-grid">
			<div class="agile-grids">	
				<!-- input-forms -->
				<div class="grids">
					<div class="progressbar-heading grids-heading">
						<h2>SubCategory</h2>
					</div>
					<div class="panel panel-widget forms-panel">
						<div class="forms">
							<div class="form-grids widget-shadow" data-example-id="basic-forms"> 
								<div class="form-title">
								</div>
								<div class="form-body">
									<?php 
									include("../config.php");

									$subcategory_id=0;
									$sql="SELECT * FROM subcategory where subcategory_id = ".$_GET['id'];
									$result = mysqli_query($conn, $sql);									
									$row = mysqli_fetch_assoc($result);
									// print_r($row);
									?>

									
									<form name="frmsubcategory" id="frmsubcategory" enctype="multipart/form-data"> 
									<input type="hidden" name="sub_id" value="<?php echo $row['subcategory_id']; ?>">		
										<div class="form-group">
											<label for="selector1">Category Select</label>
											<div>
												
												<select id = "category_id" name = "category_id" class="form-control1">
													<option>Select Category</option>
													<?php 
													$ress = $conn->query('select * from category');			
													if($ress->num_rows){
														while ($rows = $ress->fetch_assoc()) {
															$chk = $row['category_id'] == $rows['category_id'] ? 'selected' : '';
															echo '<option value = "'.$rows['category_id'].'" '.$chk.'>'.$rows['category_name'].'</option>';
														}
													}
													?>
												</select>
											</div>
										</div>
										
										
										<div class="form-group"> 
											<label for="exampleInputPassword1">SubCategory </label> 
											<input type="text" name="subcategoryname" class="form-control" id="subcategoryname" placeholder="Enter Subategory" value="<?php echo $row['subcategoryname']; ?>"> 
										</div>  
										<div class="form-group"> 
											<label for="exampleInputPassword1">Sub Category Image</label> 											
											<div class="text-center">
												<img class = "id-card" src="../<?php echo $row['sub_img']; ?>" id = "id-cardPreview">
												<div class="clearfix"></div>
												<input type = "file" id ="id-card" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name="add_card">
												<label class="btn btn-success" for = "id-card">Choose File</label>
											</div>
										</div>  
										<center><font color="black"><div id="success_message" style="display:none;">Data Updated Sucessfully </div></font></center>
										<button type="submit" id="btnadd" class="btn btn-primary w3ls-button">Submit</button> 
									</form> 
								</div>
							</div>
						</div>
					</div>
					
				</div>		

				<!-- //input-forms -->
			</div>
		</div>
		<script src="../admin/js/common.js"></script>
		<script type="text/javascript">
			imageUpload('id-card');			
		</script>
		<script src="../admin/js/up-sub-category.js"></script>

		<!-- footer -->
		<?php require("footer.php") ?>
		<!-- //footer -->
	</section>
<!-- 	<script src="js/bootstrap.js"></script>
-->	
<script type="text/javascript">
</script>
</body>
</html>

