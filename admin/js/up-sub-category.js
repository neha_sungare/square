$('#frmsubcategory').on('submit', function(e){
    e.preventDefault();

    $('#success_message').fadeIn().html('Checking Data');
    $("#btnadd").prop('disabled', true);
    var fd = new FormData(this);
    $.ajax({
        url: 'up_sub_category.php',
        type: 'POST',
        data:  fd,
        contentType: false,
        processData:false,
        success: function(data) 
        {
           // alert(data);
            // console.log(data);
            $('#success_message').fadeIn().html('Data Updated sucessfully');
            setTimeout(function() {
                $('#frmsubcategory')[0].reset();
                $("#btnadd").prop('disabled', false);
                $('#success_message').fadeOut("slow");
            }, 1000 );
        },
    });
});