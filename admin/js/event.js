
$('#frmevent').on('submit', function(e){
    e.preventDefault();
//alert(e);
$('#success_message').fadeIn().html('Checking Data');
$("#btnadd").prop('disabled', true);
var fd = new FormData(this);
fd.append('event_file',CKEDITOR.instances['event_file'].getData());
$.ajax({
    url: 'add_event.php',
    type: 'POST',
    data:  fd,
    contentType: false,
    processData:false,
    success: function(data) 
    {
         // console.log(data);
            $('#success_message').fadeIn().html('Data Submitted sucessfully');
           setTimeout(function() {
               $('#frmevent')[0].reset();
                $("#btnadd").prop('disabled', false);
                $('#success_message').fadeOut("slow");
            }, 1000 );
        },
    });
});