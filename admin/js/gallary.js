$('#frmgallary').on('submit', function(e){
    e.preventDefault();
//alert(e);
$('#success_message').fadeIn().html('Checking Data');
$("#btnadd").prop('disabled', true);

var fd = new FormData(this);
fd.append('description',CKEDITOR.instances['des'].getData());
$.ajax({
    url: 'add_gallary.php',
    type: 'POST',
    data:  fd,
    contentType: false,
    processData:false,
    success: function(data) 
    {
        //console.log(data);
          
            $('#success_message').fadeIn().html('Data Submitted sucessfully');
            setTimeout(function() {
                $('#frmgallary')[0].reset();
                $("#btnadd").prop('disabled', false);
                $('#success_message').fadeOut("slow");
            }, 1000 );
        },
    });
});