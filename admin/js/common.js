var height,width;

function imageUpload(id) {
	
	$('#'+id).on("change", function(){
		var files = !!this.files ? this.files : [];

		if (!files.length || !window.FileReader) return;

		if (/^image/.test( files[0].type)){
			var reader = new FileReader();
			reader.readAsDataURL(files[0]);            
			reader.onloadend = function(upImg){

				$("#"+id+"Preview").attr("src", this.result);
				urlValue = this.result;

				var image = new Image();
				image.src = upImg.target.result;

				image.onload = function() {
					height = this.height;
					width = this.width;					
				};
			}                
		}
	});
}