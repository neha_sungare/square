<?php include 'config.php';?>
<!DOCTYPE html>
<html>
<head>
	<title>Squre1 </title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="shortcut icon" type="image/x-icon" href="images/fav.png">
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- bootstrap-css -->
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<!--// bootstrap-css -->
	<!-- css -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<!--// css -->
	<!-- font-awesome icons -->
	<link href="css/font-awesome.css" rel="stylesheet"> 
	<!-- //font-awesome icons -->

	<!-- //font -->
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
	</script>	
	<!--animate-->
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<script src="js/wow.min.js"></script>
	<script>
		new WOW().init();
	</script>
	<!--//end-animate-->
</head>
<body>
	<!-- header -->
	<div class="header">
		<div class="top-header">
			<div class="container">
				<div class="top-header-info">
					<div class="top-header-left wow fadeInLeft animated" data-wow-delay=".5s">
						<p>We Create. You Experience.</p>
					</div>
					<div class="top-header-right wow fadeInRight animated" data-wow-delay=".5s">
						<div class="top-header-right-info">
						</div>
						<div class="social-icons">
							<ul>
								<li><a class="twitter facebook" href="https://www.facebook.com/square1eventsandmedia/"><i class="fa fa-facebook"></i></a></li>
								<li><a class="twitter" href="https://www.instagram.com/square1events"><i class="fa fa-instagram"></i></a></li>
								<li><a class="twitter" href="https://www.linkedin.com/company/square-1-events-and-media/"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
		</div>
		<div class="bottom-header">
			<div class="container">
				<div class="logo wow fadeInDown animated" data-wow-delay=".5s">
					<h1><a href="index.php"><img src="images/logo.jpg" alt="" /></a></h1>
				</div>
				<div class="top-nav wow fadeInRight animated" data-wow-delay=".5s">
					<nav class="navbar navbar-default">
						<div class="container">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">Menu						
							</button>
						</div>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li><a href="index.php" class="active">Home</a></li>
								<li><a href="about.php">About us</a></li>
								<li><a  href="service.php">Event Services</a></li>
								<li><a href="gallery.php" >Gallery</a>

								</li>	
								<li><a href="client.php">Clients</a></li>

								<li><a href="contact.php">Contact</a></li>
							</ul>	
							<div class="clearfix"> </div>
						</div>	
					</nav>		
				</div>
			</div>
		</div>
	</div>
	<!-- //header -->
	<!-- banner -->
	<div class="">
		<div class="slider">

			<script src="js/responsiveslides.min.js"></script>
			<script>
						// You can also use "$(window).load(function() {"
						$(function () {
						// Slideshow 4
						$("#slider3").responsiveSlides({
								// auto: true,
								pager: true,
								nav: false,
								speed: 500,
								namespace: "callbacks",
								before: function () {
									$('.events').append("<li>before event fired.</li>");
								},
								after: function () {
									$('.events').append("<li>after event fired.</li>");
								}
							});				
					});
				</script>
				<div id="top" class="callbacks_container-wrap">
					<ul class="rslides" id="slider3">
						<?php 
						$res = $conn->query('select * from slider');
						while ($row = $res->fetch_assoc())
						{
							?>
							<li style="background-image: url('<?php echo 'admin/'.$row['slider_image']; ?>');">
								<div class="slider-info">									
									<div class="more-button wow fadeInRight animated" data-wow-delay=".5s">

									</div>
								</div>
							</li>							
							<?php
						}
						?>

						
					</ul>
				</div>
			</div>
		</div>

		<div class="information w3lagile">
			<div class="container">
				<div class="information-heading">
					<h3 class="wow fadeInDown animated" data-wow-delay=".5s">Who We Are?</h3>
					<div class="border-line2"></div>
					<p class="wow fadeInUp animated" data-wow-delay=".5s">Square1 Events and Media is a fast evolving Event Management company which focuses on
						creating solutions that achieve your objective on time, within budget and in a memorable
						way
						. We work on an array of Corporate, Entertainment, Social Events, Conference, Seminars,
						Corporate Outing and offer complete Event Planning, Management, Promotion and Branding
						Services
						. We understand that an event is in reality a Brand Projection, Positioning and Promotion
						opportunity
						. It is the ideal platform to engage with your target audience effectively
						. Hence we create an experience that will be a different and denes you in the best possible
						way .</p>
						<div class="clearfix"></div>
						<div class="col-md-3">

						</div>
						<div class="col-md-5">
							<img class="img1" src="images/ab.png" style="margin-top:15px;">
						</div>
						<div class="col-md-3"></div>
					</div>


					<div class="clearfix"></div>

					<div class="information-grids agile-info">
					<?php 
					$rei = $conn->query('select * from intraction');
					$i = 0;
					while ($roi = $rei->fetch_assoc())
					{
						if ($i%2==0) {
							?>
							<div class="col-md-4 information-grid wow fadeInLeft animated" data-wow-delay=".5s">
								<div class="information-info">
									<div class="information-grid-img">
										<img src="<?php echo 'admin/'.$roi['intraction_image']; ?>" alt="" />
									</div>
									<div class="information-grid-info scroll style-1">
										<h4><?php echo $roi['intraction_name']; ?> </h4>
										<p><?php echo $roi['intraction_desc']; ?></p>
									</div>
								</div>
							</div>
							<?php
						}else{
							?>
							<div class="col-md-4 information-grid wow fadeInUp animated" data-wow-delay=".5s">
								<div class="information-info">
									<div class="information-grid-info scroll">
										<h4><?php echo $roi['intraction_name']; ?> </h4>
										<p><?php echo $roi['intraction_desc']; ?></p>
									</div>
									<div class="information-grid-img">
										<img src="<?php echo 'admin/'.$roi['intraction_image']; ?>" alt="" />
										
									</div>
								</div>
							</div>
							<?php
						}
						$i++;
						?>
												
						<?php
					}
					?>
						
						
						<div class="clearfix"> </div>
					</div>
				</div>
			</div>
			<!-- //information -->

			<!-- popular -->
			<div class="popular agile-info">
				<div class="container">
					<div class="popular-heading information-heading">
						<h3 style="color: #fff;" class="wow fadeInDown animated" data-wow-delay=".5s">Why People love us ..</h3>
						<div class="border-line2"></div>
					</div>

					<div class="popular-grids wthree">
						<head>
							<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
						</head>

						<section id="carousel">    				
							<div class="container">
								<div class="row">
									<div class="col-md-8 col-md-offset-2">
										<div class="quote"><i class="fa fa-quote-left fa-4x"></i></div>
										<div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="3000">
											<!-- Carousel indicators -->
											<ol class="carousel-indicators">
												<?php 
												$ref = $conn->query("select * from feedback WHERE display_status='1'");
												$k = 0;
												while ($rof = $ref->fetch_assoc()) {
													?>
													<li data-target="#fade-quote-carousel" data-slide-to="<?php echo $k; ?>"></li>

													<?php
													$k++;
												}
												?>

											</ol>
											<!-- Carousel items -->
											<div class="carousel-inner">
												<?php 
												$ref = $conn->query("select * from feedback WHERE display_status='1'");
												$k = 0;

												while ($rof = $ref->fetch_assoc()) {
													?>
													<div class="<?php echo $k == 0 ? 'active' : ''; ?> item">
														<div class="profile-circle" ">
															<img width="100" src="<?php echo 'admin/'.$rof['feed_image'];?>">
														</div>
														<blockquote>
															<p><?php echo $rof['feed_msg'];?></p>
														</blockquote>	
													</div>						
													<?php
													$k++;
												}
												?>

<!-- 				    <div class="item">
                        <div class="profile-circle" "><img src="images/pp2.png"></div>
				    	<blockquote>
				    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.<b style="color;red;">(Please provide Content here)</b></p>
				    	</blockquote>
				    </div>
				    <div class="active item">
                        <div class="profile-circle" ;"><img src="images/pp3.png"></div>
				    	<blockquote>
				    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.<b style="color;red;">(Please provide Content here)</b></p>
				    	</blockquote>
				    </div>
                    <div class="item">
                        <div class="profile-circle" ><img src="images/pp4.png"></div>
    			    	<blockquote>
				    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.<b style="color;red;">(Please provide Content here)</b></p>
				    	</blockquote>
				    </div>
                    <div class="item">
                        <div class="profile-circle" ><img src="images/pp5.png"></div>
    			    	<blockquote>
				    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.<b style="color;red;">(Please provide Content here)</b></p>
				    	</blockquote>
				    </div>
                    <div class="item">
                        <div class="profile-circle" ><img src="images/pp6.png"></div>
    			    	<blockquote>
				    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.<b style="color;red;">(Please provide Content here)</b></p>
				    	</blockquote>
				    </div> -->
				</div>
			</div>
		</div>							
	</div>
</div>
</section>
</div>
</div>
</div>
<!-- //popular -->

<!-- footer -->

<div class="footer agileits">
	<div class="container">
		<div class="footer-grids w3l-agile">
			<div class="col-md-3 footer-nav agile-w3layouts wow fadeInLeft animated" data-wow-delay=".5s">
				<h4>Quick Links</h4>
				<ul>
					<li><a href="index.php" class="">Home</a></li>
					<li><a href="about.php">About us</a></li>
					<li><a  href="service.php">Event Services</a></li>
					<li><a href="gallery2.php" >Gallery</a>

					</li>	
					<li><a href="client.php">Clients</a></li>
					
					<li><a href="contact.php">Contact</a></li>
				</ul>
			</div>
			<div class="col-md-5 footer-nav agile-w3layouts wow fadeInUp animated" data-wow-delay=".5s">
				<h4>Let's stay in touch</h4>
				<p>Subscribe for updates, special offers and more.</p>
				<form action="#" method="post">
					<input type="email" id="mc4wp_email" name="email" placeholder="Enter your email here" required="">
					<input type="submit" value="Subscribe">
				</form>
			</div>
			<div class="col-md-4 footer-nav agile-w3layouts wow fadeInRight animated" data-wow-delay=".5s">
				<img class="img1" src="images/foo.png">
			</div>
			<div class="clearfix"> </div>
		</div>

	</div>

</div>
<!-- //footer -->
<div class="copyright " data-wow-delay=".5s">
	<p>© 2018 Square 1 . All Rights Reserved . Design by <a href="http://sungare.com/"> Sungare technologies</a></p>
</div>
</body>	
</html>