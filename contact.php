<!DOCTYPE html>
<html>
<head>
	<title>Squre1 </title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="" />
	<link rel="shortcut icon" type="image/x-icon" href="images/fav.png">
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- bootstrap-css -->
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<!--// bootstrap-css -->
	<!-- css -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<!--// css -->
	<!-- font-awesome icons -->
	<link href="css/font-awesome.css" rel="stylesheet"> 
	<!-- //font-awesome icons -->

	<!-- //font -->
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
	</script>	
	<!--animate-->
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<script src="js/wow.min.js"></script>
	<script>
		new WOW().init();
	</script>
	<!--//end-animate-->
</head>
<body>
	<div class="header">
		<div class="top-header">
			<div class="container">
				<div class="top-header-info">
					<div class="top-header-left wow fadeInLeft animated" data-wow-delay=".5s">
						<p>We Create .You Experience.</p>
					</div>
					<div class="top-header-right wow fadeInRight animated" data-wow-delay=".5s">
						<div class="top-header-right-info">
							
							<!-- <ul>
								<li><a href="login.html">Login</a></li>
								<li><a href="signup.html">Sign up</a></li>
							</ul> -->
						</div>
						<div class="social-icons">
							<ul>
								<li><a class="twitter facebook" href="https://www.facebook.com/square1eventsandmedia/"><i class="fa fa-facebook"></i></a></li>
								<li><a class="twitter" href="https://www.instagram.com/square1events"><i class="fa fa-instagram"></i></a></li>
								<li><a class="twitter" href="https://www.linkedin.com/company/square-1-events-and-media/"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
		</div>
		<div class="bottom-header">
			<div class="container">
				<div class="logo wow fadeInDown animated" data-wow-delay=".5s">
					<h1><a href="index.php"><img src="images/logo.jpg" alt="" /></a></h1>
				</div>
				<div class="top-nav wow fadeInRight animated" data-wow-delay=".5s">
					<nav class="navbar navbar-default">
						<div class="container">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">Menu						
							</button>
						</div>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li><a href="index.php" >Home</a></li>
								<li><a href="about.php">About us</a></li>
								<li><a  href="service.php">Event Services</a></li>
								<li><a href="gallery.php" >Gallery</a>

								</li>	
								<li><a href="client.php">Clients</a></li>

								<li><a href="contact.php " class="active">Contact</a></li>
							</ul>	
							<div class="clearfix"> </div>
						</div>	
					</nav>		
				</div>
			</div>
		</div>
	</div>
	
	<!-- contact -->
	<div class="about-one-wrap w3layouts-agile">
		<div class="container">
			<div class="contact-heading w3layouts">
				<h2>Contact</h2>
				<div class="border-line2"></div>
			</div>
			
			
			<div class="address w3l-agileinfo">
				<div class="col-md-6 address-grids wow fadeInLeft animated" data-wow-delay=".5s">
					<h4>Address :</h4>
					<ul>
						<li><b><p>Flat no.4,Srujan,Plot no.12,Vedantnagari, </p>
							<p>Karvenagar, Pune-411 052, India</p></b>
						</li>
					</ul>
					<hr>
				</div>

				<div class="col-md-6 address-grids wow fadeInRight animated" data-wow-delay=".5s">
					<h4>Email :</h4>
					<b><p><a href="mailto:events.square1@gmail.com">events.square1@gmail.com</a></p></b>
										<b><p><a href="mailto:events.square1@gmail.com">nayaneesh@square1em.com,</a></p></b>

					<b><p><a href="mailto:events.square1@gmail.com">mayur@square1em.com</a></p></b>
	<hr>
				</div>
				<div class="col-md-6 address-grids wow fadeInRight animated" data-wow-delay=".5s">
					<h4>Contact No :</h4>
					<b><p><a href="mailto:events.square1@gmail.com">+91 95610 97096</a></p></b>
										<b><p><a href="mailto:events.square1@gmail.com">+91 99222 70020</a></p></b>

					<b><p><a href="mailto:events.square1@gmail.com">+91 8459412659</a></p></b>
	<hr>
				</div>
				<div class="clearfix"> </div>
			</div>

			<div class="contact-form col-md-6 col-xs-12 wow fadeInUp animated" data-wow-delay=".5s">

				<form name="contactfrm" id="contactfrm" action="#" method="post">
					<input type="text" name="name" id="name" placeholder="Name" required autocomplete="off" autosave="off">	
					<span id="errorname" class="error-span"></span>	 
					<input type="email" name="email" id="email" placeholder="Email" required autocomplete="off" autosave="off">	
					<span id="erroremail" class="error-span"></span>	 
					<input type="text" name="phone" id="phone" placeholder="Telephone" required autocomplete="off" autosave="off">	
					<span id="errorphone" class="error-span"></span>	 
					<textarea name="message" id="message" placeholder="Message"  required=""></textarea>
					<center><font color="black"><div id="success_message" style="display:none;">Your Message Send Sucessfully </div></font></center>
					<button type="submit" id="btnadd" name="btnadd" class="btn1 btn-1 btn-1b">Submit</button>
				</form>
			</div>
			<div class=" contact-form map agileits col-md-6 col-xs-12 wow fadeInUp animated" data-wow-delay=".5s">

				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3783.9278404358774!2d73.81577951489231!3d18.486927487428563!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2bfddf74982fb%3A0x420d91a5d95cf647!2s1%2C+Cummins+College+Rd%2C+Karve+Nagar%2C+Pune%2C+Maharashtra+411052!5e0!3m2!1sen!2sin!4v1517919303343" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>	
		</div>
	</div>
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/contact.js"></script>
	<script type="text/javascript" src="js/custom/myjquery.js"></script>
	<!-- //contact -->

	<!-- footer -->

	<div class="footer agileits">
		<div class="container">
			<div class="footer-grids w3l-agile">
				<div class="col-md-3 footer-nav agile-w3layouts wow fadeInLeft animated" data-wow-delay=".5s">
					<h4>Quick Links</h4>
					<ul>
						<li><a href="index.php" class="">Home</a></li>
						<li><a href="about.php">About us</a></li>
						<li><a  href="service.php">Event Services</a></li>
						<li><a href="gallery.php" >Gallery</a>

						</li>	
						<li><a href="client.php">Clients</a></li>

						<li><a href="contact.php">Contact</a></li>
					</ul>
				</div>
				<div class="col-md-5 footer-nav agile-w3layouts wow fadeInUp animated" data-wow-delay=".5s">
					<h4>Let's stay in touch</h4>
					<p>Subscribe for updates, special offers and more.</p>
					<form action="#" method="post">
						<input type="email" id="mc4wp_email" name="email" placeholder="Enter your email here" required="">
						<input type="submit" value="Subscribe">
					</form>
				</div>
				<div class="col-md-4 footer-nav agile-w3layouts wow fadeInRight animated" data-wow-delay=".5s">
					<img class="img1" src="images/foo.png">
				</div>
				<div class="clearfix"> </div>
			</div>

		</div>

	</div>
	<!-- //footer -->
	<div class="copyright " data-wow-delay=".5s">
		<p>© 2018 Square 1 . All Rights Reserved . Design by <a href="http://sungare.com/"> Sungare technologies</a></p>
	</div>
</body>	
</html>